import numpy as np
import pandas as pd


def split_multivariate_dataframe_to_sequences(sequences, n_steps, verbose=True):
    X, y = list(), list()
    for i in range(len(sequences)):
        # find the end of this pattern
        end_ix = i + n_steps
        # check if we are beyond the dataset
        if end_ix > len(sequences):
            break
        # gather input and output parts of the pattern
        seq_x, seq_y = sequences[i:end_ix, :-1], sequences[end_ix - 1, -1]
        X.append(seq_x)
        y.append(seq_y)

    X = np.array(X)
    y = np.array(y)

    if verbose:
        print('Dataset entry shape {}'.format(sequences.shape))
        info_text = '{} shape after process: {}'
        print(info_text.format('Features', X.shape))
        print(info_text.format('Targets', y.shape))

    return X, y

