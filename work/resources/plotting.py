import math

import pandas as pd
import matplotlib.pyplot as plt


def plot(plot_data_generator):
    for i, (predictions, targets) in enumerate(plot_data_generator):
        df_tmp = pd.DataFrame({
            'index': [i * len(predictions) + j for j in range(len(predictions))],
            'predictions': predictions,
            'targets': targets,
        })
        
        fig, ax = plt.subplots(1, figsize=(15, 10))
        df_tmp.plot(x='index', y=['predictions', 'targets'], ax=ax)
        ax.legend(['predictions', 'real'])

        plt.show()
