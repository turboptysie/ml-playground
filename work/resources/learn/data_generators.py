import math


class DataSequence(Sequence):
    def __init__(self, x_set, y_set, train_size, validation_size):
        self.x_set, self.y_set = x_set, y_set
        self.train_size = train_size
        self.validation_size = validation_size
        self.batch_size = train_size + validation_size

    def __len__(self):
        return math.ceil(len(self.x_set) / self.batch_size)

    def __getitem__(self, index):
        min_bound, max_bound = self.get_bounds(index)
        batch_x = self.x_set[min_bound: max_bound]
        batch_y = self.y_set[min_bound: max_bound]

        return batch_x, batch_y
    
    def get_bounds(self, index):
        raise NotImplemented()
        

class TrainDataSequence(DataSequence):
    def get_bounds(self, index):
        min_bound = index * self.batch_size
        max_bound = index * self.batch_size + self.train_size
        return min_bound, max_bound
    
    
class ValidationDataSequence(DataSequence):
    def get_bounds(self, index):
        min_bound = index * self.batch_size + self.train_size
        max_bound = index * self.batch_size + self.batch_size
        return min_bound, max_bound
