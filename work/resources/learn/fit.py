from livelossplot import PlotLosses


class Fitter:
    def __init__(self, model, train_generator, validation_generator):
        self.model = model
        self.train_generator = train_generator,
        self.validation_generator = validation_generator
        
        self.plot_losses = PlotLosses()


    def fit(self):
        datasets = zip(self.train_generator, self.validation_generator)
        for train_dataset, validation_dataset in datasets:
            X_train, y_train = train_dataset
            results = model.fit(
                X_train, 
                y_train,
                validation_data=validation,
            )

            self.plot(results.history)

    def plot(self, metrics):
        unpacked_metrics = {
            key: value[0]
            for key, value in metrics.items()
        }
        self.plot_losses.update(unpacked_history)
        self.plot_losses.send() # plot
