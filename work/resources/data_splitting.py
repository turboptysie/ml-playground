def split_dataset_to_features_and_target(df, target, timestamp=None, verbose=True):
    if verbose:
        print('Finded columns:\n', '-', '\n - '.join(sorted([column for column in df.columns])))

    df = df.copy(deep=True)
    y = df[target].values.reshape(-1, 1)
    df.drop(target, inplace=True, axis=1)
    
    if timestamp:
        timestamp_values = df[timestamp].values.reshape(-1, 1)
        df.drop(timestamp, inplace=True, axis=1)

    X = df.values

    if timestamp:
        return X, y, timestamp_values
    
    else:
        return X, y


def create_train_and_test_dataset(X, y, percent=0.8, verbose=True):
    threshold = int(percent * X.shape[0])
    X_train = X[:threshold]
    X_test = X[threshold:]

    y_train = y[:threshold]
    y_test = y[threshold:]

    if verbose:
        verbose_text = (
           '{} have {} samples, which shape is {}.'
        )
        summary_text = 'Train set have {} samples, test set have {} samples'
        print(verbose_text.format('Features', len(X), X.shape))
        print(verbose_text.format('Targets', len(y), y.shape))
        print()
        print(summary_text.format(len(X_train), len(X_test)))

    return X_train, X_test, y_train, y_test
