
def swap_target_column_to_end(df, target_name):
    return df[[
        column
        for column in df.columns
        if column != target_name
    ] + [target_name]]