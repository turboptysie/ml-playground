import pandas as pd


def get_data_between_dates(
    df,
    start_date=pd.Timestamp(2019, 9, 1), 
    end_date=pd.Timestamp(2020, 9, 1),
    column='timestamp',
    date_format='%Y-%m-%d %H:%M:%S',
):
    df = df.copy(deep=True)
    df[column] = pd.to_datetime(df[column], format=date_format)
    mask = (df[column] > start_date) & (df[column] <= end_date)
    return df.loc[mask].reset_index(drop=True)
    
