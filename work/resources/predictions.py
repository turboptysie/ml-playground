import numpy as np


class StandardPredictor:
    def __init__(self, model, y_scaler):
        self.model = model
        self.y_scaler = y_scaler
        
    def predict(
        self, 
        X_test,
        samples=200,
        fit_model=False,
        generator=True,
        y_test=None,
        verbose=True
    ):
        if verbose:
            print('Size of dataset using for predictions: {}.'.format(X_test.shape[0]))

        if generator and not len(y_test):
            raise ValueException('When generator flag is set to True, you should pass y_test too.')
        
        if generator:
            if len(y_test) != len(X_test):
                raise ValueException('Length of y_test should be equal to length of X_test.')
     
        predictions, targets = [], []
        for i, (features, target) in enumerate(zip(X_test, y_test)):
            x_input = features.reshape(([1] + list(X_test.shape[1:])))
            predictions.append(self.model.predict(x_input, verbose=0))
            targets.append(target)

            if fit_model:
                y_input = target.reshape(1)
                self.model.fit(x_input, y_input, verbose=0)

            if i and i % samples == 0:
                if verbose:
                    print(i)
                
                if generator:
                    yield self._inverse_transform(self.y_scaler, predictions, targets)
                    predictions.clear()
                    targets.clear()

        yield self._inverse_transform(self.y_scaler, predictions, targets)

    def _inverse_transform(self, y_scaler, predictions, targets): 
        predictions = self._prepare_y(predictions)
        targets = self._prepare_y(targets)
        predictions = y_scaler.inverse_transform(predictions).flatten()
        targets = y_scaler.inverse_transform(targets).flatten()

        return predictions, targets    

    def _prepare_y(self, y):
        return np.array(y).flatten().reshape(-1, 1)
