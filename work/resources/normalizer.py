from sklearn.preprocessing import (
    MinMaxScaler,
    StandardScaler,
)


class Normalizer:
    def normalize(self, X_train, X_test, y_train, y_test):
        X_train = self.x_scaler.fit_transform(X_train)
        X_test = self.x_scaler.transform(X_test)

        y_train = self.y_scaler.fit_transform(y_train)
        y_test = self.y_scaler.transform(y_test)

        scalers = self.x_scaler, self.y_scaler
        datasets = X_train, X_test, y_train, y_test
        
        return scalers, datasets


class StandardNormalizer(Normalizer):
    def __init__(self, x_scaler=None, y_scaler=None):
        self.x_scaler = x_scaler if x_scaler else StandardScaler()
        self.y_scaler = y_scaler if y_scaler else StandardScaler()


class MinMaxNormalizer(Normalizer):
    def __init__(self, x_scaler=None, y_scaler=None):
        self.x_scaler = x_scaler if x_scaler else MinMaxScaler()
        self.y_scaler = y_scaler if y_scaler else MinMaxScaler()
