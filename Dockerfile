FROM jupyter/datascience-notebook

COPY requirements.txt .

RUN python -m pip install -r requirements.txt

